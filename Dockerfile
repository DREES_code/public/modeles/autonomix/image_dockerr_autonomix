FROM r-base

RUN apt-get update && \
    apt-get -y install curl && \
    curl -L https://github.com/jgm/pandoc/releases/download/2.16.2/pandoc-2.16.2-1-amd64.deb -o pandoc.deb && \
    dpkg -i pandoc.deb

RUN Rscript -e "install.packages(install.packages(c('bookdown', 'mime', 'dplyr', 'knitr', 'data.table', 'survey', 'descr', 'Hmisc', 'openxlsx', 'ggplot2', 'reldist', 'stringr', 'readxl', 'haven'), repos='http://cran.rstudio.com'))"
